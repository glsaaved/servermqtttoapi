/**
 * Created by warlink on 29-11-16.
 */
var mqtt = require('mqtt');
var tryparse = require('tryparse');
var Client = require('node-rest-client').Client;
var local = "Valparaiso";
var idLocal;
var client  = mqtt.connect('mqtt://192.168.0.106');
var API = "https://quiet-badlands-18006.herokuapp.com/";
var actual = [];
var diff = [];
var id = [];
var client_ins = [];
var price =[];
var precision=5;
var client_api = new Client();
var total_temp=[];


client.on('connect', function () {
    console.log("Servidor iniciando, espere...");
    client.subscribe(['ingredientes/+']);
    client.subscribe(['cliente/+']);
    var args ={
        parameters:{"location":local},
        headers: { "Content-Type": "application/json" }
    };
    getDataIDAPI(args,"locals",function (local_id){
        idLocal=local_id;
        args ={
            parameters:{"local_id":local_id },
            headers: { "Content-Type": "application/json" }
        };
        getDataAPI(args,"ingredient_locals",function(ingredientes){
            ingredientes.data.forEach(function(item) {
                id[item.ingredient_id.name]=item.ingredient_id._id;
                actual[item.ingredient_id.name]=item.current_amount;
                diff[item.ingredient_id.name]=0;
                client_ins[item.ingredient_id.name]=0;
                price[item.ingredient_id.name]=item.price;
                total_temp[item.ingredient_id.name]=0;
            });
            console.log("Servidor Listo para uso");
            console.log("==================================");
        });
    });


});

client.on('message', function (topic, message) {
    // message is Buffer
    if(ifpost(topic,message)==1)
    {
        console.log(topic.toString()+':'+message.toString());
        var aux=tryparse.int(message);
        var ing=SplitString(topic,1);
        console.log('Diferencia:'+diff[ing]);
        args={
            parameters:{"ingredient_id":id[ing],"local_id":idLocal },
            headers: { "Content-Type": "application/json" }
        };
        getDataIDAPI(args,"ingredient_locals",function(_id){
            args = {
                data:{
                    "current_amount": aux
                },
                parameters:{"_id":_id},
                headers: { "Content-Type": "application/json" }
            };
            var dir="ingredient_locals";
            posting(args, dir,function(){
                client.publish("subtotal/"+ing, subtotal(ing)+" ");
                client.publish("total/"+ing, total(ing)+" ");
            })

        })
    }
    else if(ifpost(topic, message)==2)
    {
        var ing=SplitString(topic,1);
        if(client_ins[ing]==0||client_ins[ing]!=message)
        {
            console.log("ha llegado nuevo cliente");
            //var args={
                //parameters:{"name":"activo" },
              //  headers: { "Content-Type": "application/json" }
            //};
            //getDataAPI(args,"state_type",function(dt){
                //console.log("ha llegado nuevo cliente2");

                //if (!dt.data||dat.data.length==0){
                 //   posting(args,"state_type");
                //}
                var args={
                    parameters:{"card_id":client_ins[ing] },
                    headers: { "Content-Type": "application/json" }
                };
                getDataAPI(args,"order",function(dtOrder){
                    var _id_order=dtOrder.data[dtOrder.data.length-1]._id;
                    console.log("ha llegado nuevo cliente3");
                    if(!dtOrder){
                        posting(args,"order");
                        getDataIDAPI(args,"order",function(_id_order2) {
                            _id_order=_id_order2;
                        });
                    }
                    args={
                        parameters:{"ingredient_id":id[ing],"local_id":idLocal },
                        headers: { "Content-Type": "application/json" }
                    };
                    getDataIDAPI(args,"ingredient_locals",function(_id) {
                        args = {
                            data: {
                                "order_id": _id_order,
                                "ingredient_local_id": _id,
                                "quantity": diff[ing]
                                },
                                parameters: {"_id": _id},
                                headers: {"Content-Type": "application/json"}

                        };
                        var dir="item";
                        posting(args, dir,function(){
                            client.publish("subtotal/"+ing, o);
                            dir="order";
                            var t=Total(ing);
                            args = {
                                data: {
                                    total:t
                                },
                                parameters: {"_id": _id_order},
                                headers: {"Content-Type": "application/json"}

                            };
                            posting(args,dir,function(){
                                client.publish("total/"+ing, "0");
                            })
                        });
                    });
                });
            //});
            client_ins[ing]=message;
            diff[ing]=0;
            var args={
                parameters:{"name":"activo" },
                headers: { "Content-Type": "application/json" }
            };
            getDataIDAPI(args,"state_type",function(_id_state) {
                var args = {
                    parameters: {"card_id": client_ins[ing], "state_id": _id_state},
                    headers: {"Content-Type": "application/json"}
                };
                getDataAPI(args, "order", function (data) {
                    total_temp[ing] = data.data[0].total;
                    client.publish("total/" + ing, "0");
                });
            });
            //actualizar total
        }
    }
});

var ifpost=function(topic,message){
    if(topic.toString().startsWith('ingredientes/')){
        var ing=SplitString(topic,1);
        var aux=tryparse.int(message);
        if (aux&&Math.abs(aux-actual)>precision) {
            diff[ing]+=actual[ing]-aux;
            actual[ing]=aux;
            return 1;
        }
    }
    else if(topic.toString().startsWith('cliente/')){
        return 2;
    }
    return 0;
};
var posting=function(args, dir,callback){
    client_api.put(API+dir, args, function (data, response) {
    if (response){
        console.log("data enviada");
        callback();
    }
    else
    {
        console.log("error al enviar data");
        callback();
    }
})
};
// function you can use:
var SplitString=function (topic,n) {
    return topic.split('/')[n];
};
var getDataAPI=function (args, dir,callback){
    client_api.get(API+dir,args,function(data,response){
        if(response){
            console.log(data);
            callback (data);
        }
        else{
            console.log("Error al obtener datos");
            callback();
        }
    })
};
var getDataIDAPI=function (args, dir,callback){
    client_api.get(API+dir,args,function(data,response){
        if(data){
            console.log(data.data[0]._id);
            callback (data.data[0]._id);
        }
        else{
            console.log("Error al obtener datos");
            callback();
        }
    })
}
var subtotal=function(ing){
    return diff[ing]*price[ing]/100;
}

var total=function(ing){
    return subtotal(ing)+total_temp[ing];
}
